package it.cnr.iit.logcontroller.restapi;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModel;
import it.cnr.iit.log.aggregator.Aggregator;
import it.cnr.iit.log.aggregator.Aggregators;
import it.cnr.iit.log.utils.LogConfiguration;
import it.cnr.iit.log.utils.LogOperations;
import it.cnr.iit.log.utils.Utility;

@ApiModel(value = "LogRest",
    description = "Log APIs used by Collectors and Aggregators")
@RestController
@RequestMapping("/v1")
@EnableAutoConfiguration
public class LogController {
	
	private static Logger	logger	= LogManager.getLogger(LogController.class);
	
	@Autowired
	LogConfiguration			logConfiguration;
	
	@Autowired
	Utility								utility;
	
	@Autowired
	LogOperations					logOperations;
	
	@RequestMapping(value = "streamOutLogFragments", method = RequestMethod.POST)
	public HttpEntity<String> streamOutLogFragments(
	    @RequestParam("user-file") MultipartFile[] multipartFile)
	    throws IOException {
		
		// System.out.println("LOG CONTROLLER:> Receiving fragments...");
		logger.info("LOG CONTROLLER:> Receiving fragments...");
		// looping the multipartFile array
		
		int fileCount = 0;
		
		String configPath = logConfiguration.getConfigPath();
		
		if (!utility.ifNotExistThanCreate(configPath, "/c3isp")) {
			System.exit(1);
		}
		
		// if the configuration file is missing, we notify that
		String configFile = logConfiguration.getAggregatorConfigFile();
		if (new File(configPath + "/" + configFile).exists() == false) {
			logger.error("AGGREGATOR:> aggregator-conf.json file is missing");
			// System.out.println("Error: aggregator-conf.json file is missing");
			System.exit(1);
		}
		
		File file = new File(configPath + "/" + configFile);
		String json = new String(Files.readAllBytes(file.toPath()));
		
		Aggregators aggregatorList = null;
		
		// if the file is empty we notify that
		if ((aggregatorList = (Aggregators) utility.ReadFromJson(json,
		    Aggregators.class)) == null) {
			logger.error("AGGREGATOR:> The configuration file conf.json is empty");
			System.exit(1);
		}
		
		Aggregator aggregator = aggregatorList.getAggregators().get(0);
		
		for (MultipartFile mf : multipartFile) {
			
			// Getting a whole file
			byte[] bytes = mf.getBytes();
			if (bytes.length == 0) {
				logger.info("LOG CONTROLLER:> The file" + mf.getOriginalFilename()
				    + " is empty");
				continue;
			}
			
			InputStream is = null;
			BufferedReader bfReader = null;
			
			try {
				
				// Reading only the first line of the file
				is = new ByteArrayInputStream(bytes);
				bfReader = new BufferedReader(new InputStreamReader(is));
				String strLine = null;
				
				if ((strLine = bfReader.readLine()) != null) {
					
					// Getting all the parameters present into the first line
					// (dataType, startTime, organization)
					String[] params = strLine.split("; ");
					String[] dataTypeArray = params[0].substring(1).split("=");
					String dataType = dataTypeArray[1];
					String[] startTimeArray = params[1].split("=");
					String startTime = startTimeArray[1];
					String[] orgArray = params[3].split("=");
					String org = orgArray[1];
					String[] hostArray = params[4].split("=");
					String hostname = hostArray[1];
					String filepath = aggregator.getRootPath();// +"/"+dataType+"/"+startTime+".fgt";
					
					// create folders
					filepath += "/" + org + "/" + hostname + "/" + dataType;
					utility.ifNotExistThanCreate(filepath, filepath);
					
					// creating the file inside the previous folder
					filepath += "/" + startTime + ".fgt";
					System.out.println("size: " + mf.getSize());
					CreateFragment(mf, filepath);
					
					fileCount++;
					logger.info("LOG CONTROLLER:> Fragment " + filepath + " received!");
					// System.out
					// .println("LOG CONTROLLER:> Fragment " + filepath + " received!");
				} else {
					logger.info("LOG CONTROLLER:> The file \"\n"
					    + "					    + mf.getOriginalFilename() + \" is empty");
					// System.out.println("LOG CONTROLLER:> The file "
					// + mf.getOriginalFilename() + " is empty");
					continue;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return new ResponseEntity<String>("LOG CONTROLLER:> IO Exception",
				    HttpStatus.INTERNAL_SERVER_ERROR);
			} finally {
				try {
					if (is != null)
						is.close();
				} catch (Exception ex) {
					return new ResponseEntity<String>("LOG CONTROLLER:> IO Exception",
					    HttpStatus.INTERNAL_SERVER_ERROR);
				}
			}
		}
		
		return new ResponseEntity<String>(
		    "LOG CONTROLLER:> Uploaded " + fileCount + " fragment succesfully",
		    HttpStatus.OK);
	}
	
	public void CreateFragment(MultipartFile mf, String path) {
		FileOutputStream fileOutputStream = null;
		try {
			File uploadedFile = new File(path);
			uploadedFile.createNewFile();
			fileOutputStream = new FileOutputStream(uploadedFile);
			fileOutputStream.write(mf.getBytes());
			fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(value = "sendHeartbeat", method = RequestMethod.POST)
	public void sendHeartbeat(@RequestBody String collectorId) {
		logger.info("LOG CONTROLLER:> Heartbeat received from " + collectorId
		    + " at " + (new Date().toString()));
		// System.out.println("LOG CONTROLLER:> Heartbeat received from " +
		// collectorId
		// + " at " + (new Date().toString()));
	}
}
