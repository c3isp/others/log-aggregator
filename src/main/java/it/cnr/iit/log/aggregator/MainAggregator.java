package it.cnr.iit.log.aggregator;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import it.cnr.iit.log.rest.types.Metadata;
import it.cnr.iit.log.utils.LogConfiguration;
import it.cnr.iit.log.utils.LogOperations;
import it.cnr.iit.log.utils.Utility;

public class MainAggregator implements Runnable {

	static long countFragment = 0;
	private static Logger logger = LogManager.getLogger(MainAggregator.class);

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	@Autowired
	private LogConfiguration logConfiguration;

	@Autowired
	private LogOperations logOperations;

	@Autowired
	private Utility utility;

	@Override
	public void run() {
		// TODO Auto-generated method stub

		logger.info("AGGREGATOR:> The aggregator is running!");
		// System.out.println("The aggregator is running!");
		Aggregators aggregatorList = null;

		File file;
		String json = "";

		try {

			// creating .config/c3isp folder if it doesn't exist
			String configPath = logConfiguration.getConfigPath();
			if (!utility.ifNotExistThanCreate(configPath, "/c3isp")) {
				System.exit(1);
			}

			// if the configuration file is missing, we notify that
			String configFile = logConfiguration.getAggregatorConfigFile();
			if (new File(configPath + "/" + configFile).exists() == false) {
				logger.error("AGGREGATOR:> aggregator-conf.json file is missing");
				// System.out.println("Error: aggregator-conf.json file is missing");
				System.exit(1);
			}

			// Getting aggregator-conf.json file
			// String logFolder = logConfiguration.getLogPath();
			// if (new File(logFolder).exists() == false) {
			// logOperations.CreateFolder(logFolder, logFolder);
			// }

			file = new File(configPath + "/" + configFile);
			json = new String(Files.readAllBytes(file.toPath()));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// if the file is empty we notify that
		if ((aggregatorList = utility.ReadFromJson(json, Aggregators.class)) == null) {
			logger.error("AGGREGATOR:> The configuration file conf.json is empty");
			// System.out.println("Error: The configuration file conf.json is empty");
			System.exit(1);
		}

		// Setting the aggregator parameters using the configuration file
		Aggregator aggregator = aggregatorList.getAggregators().get(0);

		utility.ifNotExistThanCreate(aggregator.getRootPath(), "/received");

		// final String dsa = aggregatorList.getAggregators().get(0).getDsa();

		while (true) {
			logger.info("AGGREGATOR:> Reading in " + aggregator.getRootPath() + " folder...");
			// System.out.println("Reading in " + rootPath + " folder...");

			try (Stream<Path> filePathStream = Files.walk(Paths.get(aggregator.getRootPath()))) {
				filePathStream.forEach(filePath -> {
					if (Files.isRegularFile(filePath)) {
						countFragment++;
						logger.info("AGGREGATOR:> found file " + filePath);

						// creating metadata
						logger.info("AGGREGATOR:> Creating Metadata...");
						Metadata metadata = logOperations.composeMetadata(aggregator.getDsaId(), filePath);
						logger.info("AGGREGATOR:> Creating Dpo for file \"" + filePath.getFileName() + "\"...");
						// calling uploadSingleFile restApi owned by the local ISI
						ResponseEntity<String> response = logOperations.createDpoLocally(filePath, metadata);

						if (response.getStatusCode().is4xxClientError()
								|| response.getStatusCode().is5xxServerError()) {
							logger.error("AGGREGATOR:> Error " + response.getStatusCodeValue());
						} else {
							logger.info("AGGREGATOR:> Dpo created correctly. Body content: " + response.getBody());
							File toDelete = new File(filePath.toString());
							toDelete.delete();
						}
						// System.out.println("found file " + filePath);
					}
				});

				logger.info("AGGREGATOR:> Total file found: " + countFragment);
				logger.info("AGGREGATOR:> going to sleep for " + aggregator.getSleepTime() + " seconds");
				// System.out.println("Total file found: " + countFragment);
				// System.out.println("The aggregator is going to sleep for "
				// + sleepTime / 1000 + " seconds");

				countFragment = 0;
				Thread.sleep(aggregator.getSleepTime() * 1000);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
