package it.cnr.iit.log.aggregator;

public class Aggregator {
	
	private String	rootPath;
	private long		sleepTime;
	private String	organization;
	private String	dsaId;
	
	public Aggregator() {
	}
	
	public String getRootPath() {
		return rootPath;
	}
	
	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}
	
	public long getSleepTime() {
		return sleepTime;
	}
	
	public void setSleepTime(long sleepTime) {
		this.sleepTime = sleepTime;
	}
	
	public String getOrganization() {
		return organization;
	}
	
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
	public String getDsaId() {
		return dsaId;
	}
	
	public void setDsaId(String dsaId) {
		this.dsaId = dsaId;
	}
	
}
