package it.cnr.iit.log.aggregator;

import java.util.List;

public class Aggregators {

	private List<Aggregator> aggregators;
	
	public Aggregators() {}

	public List<Aggregator> getAggregators() {
		return aggregators;
	}

	public void setAggregators(List<Aggregator> aggregators) {
		this.aggregators = aggregators;
	}
	
}
