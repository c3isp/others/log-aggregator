package it.cnr.iit.log.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

/**
 * This class provides some static methods to perform a rest request
 * 
 * @author antonio
 *
 */
final public class RESTUtils {
	
	private static final Logger LOGGER = Logger
	    .getLogger(RESTUtils.class.getName());
	
	private RESTUtils() {
		
	}
	
	/**
	 * Post an object to the host specified by the url parameter, expecting no
	 * returning object.
	 * 
	 * @param url
	 *          the complete url of the host
	 * @param obj
	 *          the object to send using the post http method
	 */
	public static void post(String url, Object obj) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Void> response = restTemplate.postForEntity(url, obj,
		    Void.class);
		if (response == null || !response.getStatusCode().is2xxSuccessful()) {
			LOGGER.log(Level.INFO, "FAIL: " + url);
		}
	}
	
	/**
	 * Post an object to the host specified by the url parameter, expecting a
	 * returning object of the class specified by the parameter responseType.
	 * 
	 * @param url
	 *          url the complete url of the host
	 * @param obj
	 *          obj the object to send using the post http method
	 * @param responseType
	 *          the type of the object expected to retrieve.
	 * @return an instance of the class specified by the parameter responseType if
	 *         successful, null otherwise.
	 */
	public static <T, E> T post(String url, E obj, Class<T> responseType) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<T> response = restTemplate.postForEntity(url, obj,
		    responseType);
		if (response == null || !response.getStatusCode().is2xxSuccessful()) {
			LOGGER.log(Level.INFO, "FAIL: " + url);
			return null;
		}
		return response.getBody();
	}
	
	/**
	 * Post an object to the host specified by the url parameter, expecting a
	 * returning object of the class specified by the parameter responseType.
	 * 
	 * @param url
	 *          url the complete url of the host
	 * @param obj
	 *          obj the object to send using the post http method
	 * @param responseType
	 *          the type of the object expected to retrieve.
	 * @return an instance of the class specified by the parameter responseType if
	 *         successful, null otherwise.
	 */
	public static <T, E> T get(String url, E obj, Class<T> responseType) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<T> response = restTemplate.getForEntity(url,
		    responseType, obj);
		if (response == null || !response.getStatusCode().is2xxSuccessful()) {
			LOGGER.log(Level.INFO, "FAIL: " + url);
			return null;
		}
		return response.getBody();
	}
	
	/**
	 * Post an object to the host specified by the url parameter, expecting a
	 * returning object of the class specified by the parameter responseType.
	 * 
	 * @param url
	 *          url the complete url of the host
	 * @param obj
	 *          obj the object to send using the post http method
	 * @param responseType
	 *          the type of the object expected to retrieve.
	 * @return an instance of the class specified by the parameter responseType if
	 *         successful, null otherwise.
	 */
	public static <T, E> T postAsString(String url, E obj,
	    Class<T> responseType) {
		RestTemplate restTemplate = new RestTemplate();
		String string = new Gson().toJson(obj);
		ResponseEntity<T> response = restTemplate.postForEntity(url, string,
		    responseType);
		if (response == null || !response.getStatusCode().is2xxSuccessful()) {
			LOGGER.log(Level.INFO, "FAIL: " + url);
			return null;
		}
		return response.getBody();
	}
	
	/**
	 * Post an object to the host specified by the url parameter, expecting no
	 * returning object.
	 * 
	 * @param url
	 *          the complete url of the host
	 * @param obj
	 *          the object to send using the post http method
	 */
	public static <E> void asyncPost(String url, E obj) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<E> entity = new HttpEntity<>(obj, headers);
		AsyncRestTemplate restTemplate = new AsyncRestTemplate();
		restTemplate.postForEntity(url, entity, Void.class);
	}
	
	/**
	 * Post an object to the host specified by the url parameter, expecting a
	 * returning object of the class specified by the parameter responseType.
	 * 
	 * @param url
	 *          url the complete url of the host
	 * @param obj
	 *          obj the object to send using the post http method
	 * @param responseType
	 *          the type of the object expected to retrieve.
	 * @return an instance of the class specified by the parameter responseType if
	 *         successful, null otherwise.
	 */
	public static <T, E> T asyncPost(String url, E obj, Class<T> responseType) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<E> entity = new HttpEntity<>(obj, headers);
		AsyncRestTemplate restTemplate = new AsyncRestTemplate();
		try {
			ListenableFuture<ResponseEntity<T>> response = restTemplate
			    .postForEntity(url, entity, responseType);
			if (response == null
			    || !response.get().getStatusCode().is2xxSuccessful()) {
				LOGGER.log(Level.INFO, "FAIL: " + url);
				return null;
			}
			return response.get().getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	/**
	 * Post an object to the host specified by the url parameter, expecting a
	 * returning object of the class specified by the parameter responseType.
	 * 
	 * @param url
	 *          url the complete url of the host
	 * @param obj
	 *          obj the object to send using the post http method
	 * @param responseType
	 *          the type of the object expected to retrieve.
	 * @return an instance of the class specified by the parameter responseType if
	 *         successful, null otherwise.
	 */
	public static <T, E> T asyncPostAsString(String url, E obj,
	    Class<T> responseType) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);
		AsyncRestTemplate restTemplate = new AsyncRestTemplate();
		String string = new Gson().toJson(obj);
		HttpEntity<String> entity = new HttpEntity<>(string, headers);
		try {
			ListenableFuture<ResponseEntity<T>> response = restTemplate
			    .postForEntity(url, entity, responseType);
			if (response == null
			    || !response.get().getStatusCode().is2xxSuccessful()) {
				LOGGER.log(Level.INFO, "FAIL: " + url + "\t" + string);
				return null;
			}
			return response.get().getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Post an object to the host specified by the url parameter, expecting a
	 * returning object of the class specified by the parameter responseType.
	 * 
	 * @param url
	 *          url the complete url of the host
	 * @param obj
	 *          obj the object to send using the post http method
	 * @param responseType
	 *          the type of the object expected to retrieve.
	 * @return an instance of the class specified by the parameter responseType if
	 *         successful, null otherwise.
	 */
	public static <E> void asyncPostAsString(String url, E obj) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);
		AsyncRestTemplate restTemplate = new AsyncRestTemplate();
		String string = new Gson().toJson(obj);
		HttpEntity<String> entity = new HttpEntity<>(string, headers);
		restTemplate.postForEntity(url, entity, Void.class);
	}
	
}
