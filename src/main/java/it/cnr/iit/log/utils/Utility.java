package it.cnr.iit.log.utils;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class Utility {
	
	private static Logger logger = LogManager.getLogger(Utility.class);
	
	public boolean ifNotExistThanCreate(String path, String toPrint) {
		
		if (new File(path).exists() == false) {
			logger.error("AGGREGATOR:> " + toPrint + " file is missing");
			CreateFolder(path, toPrint);
			return false;
		}
		
		return true;
		
	}
	
	public void CreateFolder(String path, String dir) {
		
		File file;
		if ((file = new File(path)) != null) {
			if (!file.isDirectory()) {
				logger.info("UTILS:> Creating \"" + dir + "\" directory...");
				file.mkdirs();
			}
		}
	}
	
	public <T> T ReadFromJson(String data, Class<T> className) {
		
		try {
			return new ObjectMapper().readValue(data, className);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
