package it.cnr.iit.log.utils;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(ignoreUnknownFields = true, ignoreInvalidFields = true)
public class LogConfiguration {
	
	@Value("${log-aggregator.config-file}")
	private String	aggregatorConfigFile;
	
	@Value("${log-aggregator.config-path}")
	private String	configPath;
	
	@Value("${log-aggregator.username}")
	private String	username;
	@Value("${log-aggregator.home}")
	private String	home;
	
	@Value("${api.endpoint.portal.uploadSingleFile}")
	private String	uploadSingleFile;
	
	public LogConfiguration() {
	}
	
	@PostConstruct
	public void init() {
		if (username == null || username.isEmpty())
			username = System.getProperty("user.name");
		if (home == null || home.isEmpty())
			home = System.getProperty("user.home");
	}
	
	public String getAggregatorConfigFile() {
		return aggregatorConfigFile;
	}
	
	public void setAggregatorConfigFile(String aggregatorConfigFile) {
		this.aggregatorConfigFile = aggregatorConfigFile;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getHome() {
		return home;
	}
	
	public void setHome(String home) {
		this.home = home;
	}
	
	public void setConfigPath(String configPath) {
		this.configPath = configPath;
	}
	
	public void setUploadSingleFile(String uploadSingleFile) {
		this.uploadSingleFile = uploadSingleFile;
	}
	
	public String getConfigPath() {
		return configPath;
	}
	
	public String getUploadSingleFile() {
		return uploadSingleFile;
	}
	
}
