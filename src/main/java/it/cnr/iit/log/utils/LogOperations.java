package it.cnr.iit.log.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URI;
import java.nio.file.Path;
import java.security.SecureRandom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import it.cnr.iit.log.rest.types.Metadata;

@Component
public class LogOperations {

	@Value("${log-aggregator.local-isi.dpos.path}")
	private String localDposPath;

	@Autowired
	public LogConfiguration logConfiguration;
	@Autowired
	public RestTemplate restTemplate;

	private static Logger logger = LogManager.getLogger(LogOperations.class);

	public LogOperations() {
	}

	public String generateUniqueFileName(String filename) {

		SecureRandom test = new SecureRandom();
		String resultStr = test.nextInt(1000000000) + "-";

		test = new SecureRandom();
		resultStr += test.nextInt(1000000000) + "-";

		test = new SecureRandom();
		resultStr += test.nextInt(1000000000) + "-";

		test = new SecureRandom();
		resultStr += test.nextInt(1000000000) + "-" + filename;

		return resultStr;
	}

	private Metadata generateMetadata(String string) {

		Metadata metadata = new Metadata();

		String metadataArray[] = string.split("#");
		metadataArray = metadataArray[1].split("; ");
		String metadataValue[] = metadataArray[0].split("=");

		metadata.setEventType(metadataValue[1]);
		metadataValue = metadataArray[1].split("=");
		metadata.setStartTime(metadataValue[1]);
		metadataValue = metadataArray[2].split("=");
		metadata.setEndTime(metadataValue[1]);
		metadataValue = metadataArray[3].split("=");
		metadata.setOrganization(metadataValue[1]);
		metadata.setDsaId("");
		metadata.setId("");

		return metadata;
	}

	public Metadata composeMetadata(String dsa, Path filePath) {

		String filename = generateUniqueFileName(filePath.getFileName().toString());

		Metadata metadata = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(filePath.toString())));

			String line;
			if ((line = br.readLine()) != null) {

				metadata = generateMetadata(line);
				metadata.setDsaId(dsa);
				metadata.setId(filename);

			}

			br.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return metadata;
	}

	public ResponseEntity<String> createDpoLocally(Path filePath, Metadata metadata) {

		ResponseEntity<String> response = null;
		try {

			FileSystemResource tmp = new FileSystemResource(filePath);
			LinkedMultiValueMap<String, Object> toUpload = new LinkedMultiValueMap<>();
			toUpload.add("metadata", metadata);
			toUpload.add("files", tmp);
			RequestEntity<MultiValueMap<String, Object>> requestEntity;

			requestEntity = RequestEntity.post(new URI(logConfiguration.getUploadSingleFile()))
					.contentType(MediaType.MULTIPART_FORM_DATA).body(toUpload);

			response = restTemplate.exchange(requestEntity, String.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(response);

		return response;

	}

}
